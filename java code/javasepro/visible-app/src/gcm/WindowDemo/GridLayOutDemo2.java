package gcm.WindowDemo;

import java.awt.*;

public class GridLayOutDemo2 {
    public static void main(String[] args) {
        Frame frame = new Frame("计算器");

        frame.add(new TextField(30),BorderLayout.NORTH);

        Panel p = new Panel();
        p.setLayout(new GridLayout(3,5,4,4));
        for (int i = 0; i < 10; i++) {
            p.add(new Button(i+""));
        }
        String s = "+-*/.";
        for(int i=0;i<5;i++){
            p.add(new Button(s.charAt(i)+""));
        }
        frame.add(p);

        frame.pack();
        frame.setVisible(true);
    }
}
