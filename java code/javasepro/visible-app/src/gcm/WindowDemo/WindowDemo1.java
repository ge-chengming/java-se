package gcm.WindowDemo;

import java.awt.* ;

public class WindowDemo1 {
    public static void main(String[] args) {
        //创建一个窗口对象
        Frame frame = new Frame("测试Window窗口");
        //指定窗口的位置和大小
        frame.setLocation(500,300);
        frame.setSize(800,800);
        //设置窗口可见
        frame.setVisible(true);
    }
}
