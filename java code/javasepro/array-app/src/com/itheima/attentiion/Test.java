package com.itheima.attentiion;

public class Test {
    public static void main(String[] args) {
        int[] arr = {11,22,33};
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);
//        System.out.println(arr[3]);//访问元素超过了最大地址
        //问题1 ：访问元素不能超过最大索引


        arr = null;
        System.out.println(arr);
//        System.out.println(arr.length);
        //arr中地址被清空，在去访问数组时寻找不到目标，出项空指针异常NullPointerException

    }
}
