package com.itheima.creat;

public class ArrayDemo4 {
    public static void main(String[] args) {
        //目标 ：学会动态初始化数组的定义和使用。
        // 语法
        // 数据类型 【】 数据名称 = new 数据类型【n】
        double[] scores = new double[3];
        //  现在存放的数据          [ 0.0 , 0.0 , 0.0 ]
        //  索引                     0     1      2
        //赋值
        scores [0] = 99.5;
        System.out.println(scores [0]);

        String[] names =new String[90];
        names[0] = "迪丽热巴";
        names[2] = "马尔扎哈";
        System.out.println(names[0]);
        System.out.println(names[1]);
        System.out.println(names[2]);
    }
}
