package com.itheima.creat;

public class ArrayDemo1 {
    public static void main(String[] args) {
        //目标 ：学会使用静态初始化的方式定义数组
        //数据类型 【】 数组名称 = new 数据类型【】 {元素1，元素2，。。。}
        double [] scores = new double [] {99.5,88.0,75.5} ;

        int [] ages = new int[]{12,24,36};

        String[] name = new String[]{"牛二","全蛋儿","老王"};
        //访问数组 发现是一个地址
        //数组是一个引用数据类型
        System.out.println(scores);
        //访问结果 [D@16b98e56
        // [是数组的意思 Ddouble的意思 16b98e56是地址

    }
}
