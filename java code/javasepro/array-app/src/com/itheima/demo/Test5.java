package com.itheima.demo;

public class Test5 {
    public static void main(String[] args) {
        //需求 ：对数组进行排序
        //1.定义一个数组，存储一些数据
        int[] arr = {5,2,3,1};
        //           0 1 2 3

        //2.定义一个循环，控制比较的轮数
        for (int i = 0; i < (arr.length - 1) ; i++) {
            //比较轮数 = 数组长度 - 1
            //   i   =  arr.length - 1
            //i == 0 第1轮     比较3次    j = 0 1 2
            //i == 1 第2轮    比较2次    j = 0 1
            //i == 2 第3轮    比较1次    j = 0
            //当前轮数 = i + 1
            //3.定义一个循环控制每轮比较的次数，占位
            for (int j = 0; j < (arr.length - (i + 1)) ; j++) {
                //比较次数 = 数组长度 - 当前轮数
                // j      = arr,length - (i + 1)
                //判断j当前位置的元素值 是否 大于后一个位置 若较大 则交换
                if(arr[j] >arr[j + 1]) {
                    int temp = arr[j + 1];
                    arr[j +1] = arr[j];
                    arr[j] = temp ;
                }
            }
        }
        //遍历数组进行元素输出
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }
    }
}
