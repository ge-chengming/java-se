package com.itheima.demo;

import java.util.Scanner;

public class ClassScoreDateAnalysisAllForMyselfBate0 {
    public static void main(String[] args) {


        //接受用户想要排序的个数和值
        System.out.println("请输入班级学生个数：");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        Scanner sc1 = new Scanner(System.in);

        //定义一个动态初始化数组，把用户想要排序的值赋予它
        System.out.println("班级各学生的分数和其分数情况");
        int[] codes = new int[number];
        for (int i = 0; i < codes.length; i++) {
            System.out.println("请输入每个学生的分数：");
            int value = sc1.nextInt();
            codes[i] = value;
        }
        for (int i = 0; i < codes.length; i++) {
            System.out.println((i + 1) + "号" + codes[i] + "分" );
            if (codes[i] < 60 ) {
                System.out.println((i + 1) + "号" + "挂科!!!!!!!!!");
            }else if (codes[i] >= 60 && codes[i] < 80){
                System.out.println((i + 1) + "号" + "不咋地");
            } else if (codes[i] >= 80 && codes[i] < 90) {
                System.out.println((i + 1) + "号" + "还可以!");
            } else if (codes[i] >= 90 && codes[i] <= 100){
                System.out.println((i + 1) + "号" + "牛逼!!!");
            } else {
                System.out.println("请输入正确的分数，别想上天！");
            }
        }

        //以上代码完成了把用户想要排序的个数和每个值




        //接下来开始冒泡排序
        //需求 ：冒泡排序
        //重点1 ：冒泡几次
        //重点2 ：每冒泡一次换几轮
        //1。确定要排几轮
        //轮数 = i + 1
        //比较的轮数 = 数组长度 - 1
        System.out.println();
        System.out.println();

        System.out.println("班级位次排名");
        for (int i = 0; i < (codes.length - 1) ; i++) {
            //每冒泡一次，要比较几次
            //比较次数等于 = 数组长度 - 当前轮数
            for (int j = 0; j < (codes.length - (i + 1)); j++) {
                if (codes[j] < codes[j + 1]){
                    int temp = codes[j + 1];
                    codes[j + 1] = codes[j];
                    codes[j] = temp;
                }
            }
        }
        System.out.println();
        for (int i = 0; i < codes.length; i++) {
            System.out.println( "第" +  (i + 1) + "位是" +  codes[i] + "分");
        }

        //以上代码完成了冒泡排序

        //接下来开始求最大值


        //1.定义一个变量用于存储一个最值元素，建议使用第一个元素作为参照
        int max = codes[0] ;

        //2.遍历数组的每个元素，依此与最大值变量的数据进行比较、若较大，则替换。
        for (int i = 0; i < codes.length; i++) {
            if (codes[i] > max) {
                max = codes[i] ;
            }
        }

        //3.输出最大值变量存储的数据即可
        System.out.println();
        System.out.println();
        System.out.println("全班最高分是：" + max + "分") ;


        //以上代码完成了求最大值


        //接下来开始求数组总和

        //定义一个求和变量累加数组的元素值
        int sum = 0 ;

        for (int i = 0; i < codes.length; i++) {
            //拿到每个元素值进行累加
            sum += codes[i] ;
        }

        //4.输出求和变量即可
        System.out.println();
        System.out.println("全班的总分是：" + sum + "分");

        //以上代码完成了数组的总和

        //接下来开始求数组的平均值
        int average = (sum / number) ;
        System.out.println();
        System.out.println("全班的平均分是：" + average + "分");


        //以上代码完成了求数组的平均值

        //接下来开始分析数组的情况

        System.out.println();
        int count = 0 ;
        for (int i = 0; i < codes.length; i++) {
            if (codes[i] >= 0 && codes[i] < 60){

                 count++ ;
            } else if (codes[i] >= 60 && codes[i] < 80){

            } else if (codes[i] >= 80 && codes[i] < 90) {

            } else if (codes[i] >= 90 && codes[i] <= 100){

            } else {
                System.out.println("请输入正确的分数，别想上天！");
            }
        }
        System.out.println();
        System.out.println( "挂科了" +  count + "人");
        //以上代码完成了数组的数据分析

        //
    }
}
