package com.itheima.memory;

public class ArrayDemo1 {
    public static void main(String[] args) {
        //目标：理解数据在内存中的存放
        int a = 12 ;
        System.out.println(a);

        int[] arr = {11,22,33};
        System.out.println(arr);

        arr[0] = 44 ;
        arr[1] = 55 ;
        arr[2] = 66 ;

        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);

    }
}
