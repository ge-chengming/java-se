package com.itheima.loop;

public class ForDemo1 {
    public static void main(String[] args) {
        //目标：学会使用for循环，并且理解它的执行流程
        //语法
        // for (初始化语句；循环条件；迭代语句){
        //          循环体语句（重复执行的代码）；
        // }

        //需求 ：输出3次helloworld
        for (int i = 0 ; i < 3 ; i ++) {
            System.out.println("helloworld");
        }

        System.out.println("------------------------------");

    }
}
