package com.itheima.loop;

public class ForTest2 {
    public static void main(String[] args) {
        //需求 ：记录1-5的和
        //1.定义一个for循环 1，2，3，4，5
        //2.定义一个整数变量用于累加数据求和
        int sum = 0 ;

        for (int i = 0; i <= 5 ; i++) {
            // i = 1 2 3 4 5
            //3.把循环数据累加给上变量
            sum += i ;
            /*
            等价于 sum = sum + 1 ;
            i == 1 sum = 0 + 1
            i == 2 sum = 1 + 2
            i == 3 sum = 3 + 3
            i == 4 sum = 6 + 4
            i == 5 sum = 10 + 5
            i == 6
             */
        }
        System.out.println(sum);

        int sum1 = 0 ;
        for (int i = 1; i <= 100 ; i++) {
            sum1 += i ;
        }
        System.out.println(sum1);
    }
}
