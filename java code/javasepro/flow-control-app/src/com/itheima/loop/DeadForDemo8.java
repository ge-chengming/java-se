package com.itheima.loop;

import java.util.Scanner;

public class DeadForDemo8 {
    public static void main(String[] args) {
        //目标；学会定义死循环

        // for 死循环
//        for ( ;  ; ) {
//            System.out.println("helloworld");
//        }

        // while 死循环
        // 经典写法
//        while (true){
//            System.out.println("我是个快乐的死循环~~~~");
//        }

        // do_while 死循环
//        do{
//            System.out.println("我是个快乐的死循环~~~~");
//        }while (true);

        System.out.println("---------------------------------------------------");
        //密码登入程序设计
        //1.定义正确的密码
        int okpassword = 520 ;
        //2.定义一个死循环让用户不断的输入密码认证
        Scanner sc = new Scanner(System.in) ;
        while (true){
            System.out.println("请您输入正确的密码；");
            int password = sc.nextInt() ;
            //3.使用if判断密码是否正确
            if (password == okpassword){
                System.out.println("登入成功了~~~");
                break;//4.break立即结束当前所在循环的执行
            }else{
                System.out.println("密码错误");
            }
        }
    }
}
