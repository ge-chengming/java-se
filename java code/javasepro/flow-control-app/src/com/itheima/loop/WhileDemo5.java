package com.itheima.loop;

public class WhileDemo5 {
    public static void main(String[] args) {
        //目标： 学会使用while循环，并且理解它的流程
        /*
        语法
        初始化语句；
        while (循环条件){
            循环体语句（被重复执行的代码）；
            迭代语句；
        }
         */
        int i = 0 ;
        while (i < 3){
            System.out.println("helloworld");
            i++ ;
        }



        System.out.println("---------------------");
        int j = 0 ;
        while (j < 3){
            System.out.println("helloworld");
        }
        //没有迭代语句，也不跳出
        //死循环



    }
}
