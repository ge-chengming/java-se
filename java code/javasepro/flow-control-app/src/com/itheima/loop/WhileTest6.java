package com.itheima.loop;

public class WhileTest6 {
    public static void main(String[] args) {
        //需求：求出纸张折叠几次厚度不低于珠穆朗玛峰的厚度
        // 珠穆朗玛峰的高度是8848860mm
        // 纸张厚度是0.1mm
        double peakHeight = 8848860 ;
        double paperThickness = 0.1 ;
        int count = 0 ;
        while (paperThickness < peakHeight){
            paperThickness *= 2 ;
            count += 1 ;
        }
        System.out.println(count);
        System.out.println(paperThickness);
    }
}
