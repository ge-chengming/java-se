package com.itheima.loop;

public class ForTest4 {
    public static void main(String[] args) {
        //需求；找出水仙花数和其个数，并且输出
        //1.定义一个for循环找出全部三位数
        //循环外定义一个变量用于记录水仙花数的个数
        int count = 0 ;

        for (int i = 100; i <= 999 ; i++) {
            //2.判断这个三位数是否满足需求
            //example 148
            //个位
            int ge = i % 10 ;
            //十位
            int shi = (i / 10) % 10 ;
            //百位
            int bai = i / 100 ;

            // 要求：每位数的立方和等于原数
            if ((ge*ge*ge + shi*shi*shi + bai*bai*bai) == i){
                System.out.print(i + "\t");
                count++ ;
            }
        }
        System.out.println();//换行
        System.out.println("水仙花数个数：" + count);
    }
}
