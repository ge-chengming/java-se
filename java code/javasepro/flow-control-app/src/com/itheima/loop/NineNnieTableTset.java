package com.itheima.loop;

public class NineNnieTableTset {
    public static void main(String[] args) {
        //需求：打印99乘法表
        //9行
        //第一行一列
        //第二行两列
        //。。。。
        //第一行1 * 1 = 1
        //第二行2 * 1 = 2 2 * 2 = 4

        for (int i = 1; i <= 9 ; i++) {
            for (int j = 1; j <= i ; j++) {
                System.out.print((j + "*" + i + "=" + (j * i)) + "\t");
            }
            System.out.println();
        }
    }
}
