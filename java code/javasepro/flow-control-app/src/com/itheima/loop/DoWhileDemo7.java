package com.itheima.loop;

public class DoWhileDemo7 {
    public static void main(String[] args) {
        //目标;学会使用dowhile循环，并理解其执行流程
        //语法
        /*
        初始化语句
        do {
            循环体语句；
            迭代语句；
        }while(循环条件)
         */

        int i = 0 ;
        do {
            System.out.println("helloworld");
            i++ ;
        }while (i < 3) ;
    }
}
