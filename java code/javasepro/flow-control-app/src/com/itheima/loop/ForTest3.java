package com.itheima.loop;

public class ForTest3 {
    public static void main(String[] args) {
        //需求 ： 求1-10的奇数和
        //方法一
        //1.定义一个循环找到1 2 3 4 .。。。 10
        //3.定义一个求和的变量
        int sum = 0 ;
        for (int i = 1; i <= 10 ; i++) {
            // i 1 2 3 4 ... 10
            //2.筛选出奇数
            if (i % 2 == 1){
                // i 1 3 5 7 9
                sum += i ;
            }
        }
        //4.输出变量即可
        System.out.println(sum);


        System.out.println("-------------------------------------");
        //方法二
        int sum1 = 0 ;
        for (int i = 1; i <= 10 ; i+=2) {
            sum1 += i ;
        }
        System.out.println(sum1);
    }
}
