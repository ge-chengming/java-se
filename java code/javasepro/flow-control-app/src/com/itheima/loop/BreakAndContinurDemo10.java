package com.itheima.loop;

public class BreakAndContinurDemo10 {
    public static void main(String[] args) {
        //目标：理解break和continue的作用。
        // 场景：假如你又有老婆了，然后你犯错了
        // 你老婆罚你做5天家务，每天都是洗碗，但是洗到第三天心软了原谅你了，你就不用洗了
        for (int i = 0; i < 5 ; i++) {
            System.out.println("快乐的洗碗~~~");
            if (i == 2){
                break;//跳出循环
            }
        }

        //countinue 跳出当前循环的当次执行，进入循环的下一次
        //场景：假如你又有老婆了，然后你犯错了
        //  你老婆罚你做5天家务，每天都是洗碗，但是洗到第三天心软了原谅你了，你就不用洗了
        //  但是依然不解恨，继续洗第四第五天
        for (int i = 1; i <= 5 ; i++) {
            if (i == 3){
                continue;//立即跳出当次执行，进入循环的下一次
            }
            System.out.println("快乐的洗碗" + i);
        }
    }
}
