package com.itheima.branch;

public class SwitchDemo4 {
    public static void main(String[] args) {
        //注意
        //存在多个case分支的功能代码是一样的时候，可以用穿透性把流程控制到一处处理，这样就可以简化代码
        //需求 用户输入月份可以展示该月的天数
        //1,3,5,7,8.10.12 月 是31天
        //2月份 闰年是29天 非闰年是28天
        //1，6，9，11月份是 30 天
        int month = 7 ;
        switch (month){
            case 1 :
                System.out.println(31);
                break;
            case 3 :
                System.out.println(31);
                break;
            case 7 :
                System.out.println(31);
                break;
            case 8 :
                System.out.println(31);
                break;
            case 10 :
                System.out.println(31);
                break;
            case 12 :
                System.out.println(31);
                break;
            case 2 :
                System.out.println("29");
                break;
            default:
                System.out.println(30);
                break;
        }
        //代码太繁杂了
        //使用switch穿透来解决问题

        //看下面代码

        switch (month) {
            case 1 :
            case 3 :
            case 5 :
            case 7 :
            case 8 :
            case 10 :
            case 12 :
                System.out.println(31);
                break;
            case 2 :
                System.out.println(29);
                break;
            case 4 :
            case 6 :
            case 9 :
            case 11 :
                System.out.println(30);
                break;
            default:
                System.out.println("数据有误");
        }
    }
}
