package com.itheima.branch;

import java.util.Scanner;

/**
 * 目标，学会使用 if 分支机构解决一些问题，理解其流程。
 */
public class LfDemo1 {
    public static void main(String[] args) {
        //需求：心跳（60~100）之间是正常的，否则系统提示进一步检查
        //格式一 ： if（条件表达式子）{代码...}
        Scanner sc = new Scanner(System.in) ;
        System.out.println("请输入你的心跳数据:");
        int heartBeat = sc.nextInt() ;
        if (heartBeat < 60 || heartBeat > 100) {
            System.out.println("您的心跳数据是：" + heartBeat + ",您可能需要进一步检查！");
        }
        System.out.println("检查结束");

        //格式二 ； if（条件表达式子）{代码。。。。}else{代码。。。}
        //需求：发红包
        Scanner sc1 = new Scanner(System.in) ;
        System.out.println("请输入你的余额：");
        double money = sc.nextDouble(); ;
        if (money >= 1314){
            System.out.println("您当前发送红包成功~~~");
        } else {
            System.out.println("自己都没钱就别发了");
        }

        //格式三： if（条件表达式）{代码块}else if（条件表达式）{代码块}....else{代码}
        //绩效系统:0~60 C 60~80 B 80~90 A 90~100 A+
        Scanner sc2 = new Scanner(System.in) ;
        System.out.println("请输入你的成绩：");
        int score = sc.nextInt();
        if (score >= 0 && score < 60){
            System.out.println('C');
        } else if (score >= 60 && score < 80){
            System.out.println('B');
        } else if (score >= 80 && score < 90) {
            System.out.println('A');
        } else if (score >= 90 && score <= 100){
            System.out.println("A+");
        } else {
            System.out.println("请输入正确的分数，别想上天！");
        }

    }
}
