package com.itheima.branch;

import java.util.Scanner;

public class SwitchDemo2 {
    public static void main(String[] args) {
        //目标 ： 学会使用switch分支结构
        /**语法 ： switch(表达式){
         *          case 值1 ：
         *              执行代码 ；
         *              break ；
     *              case 值2 ：
         *              执行代码 ；
         *              break ；
         *              .....
         *          defaoult ：
         *              执行代码；
         *      }
         */


        //需求 ： 备忘录
        //周一 ：埋头苦干，解决bug
        //周二 ：请大牛程序猿帮忙
        //周三 ： 今晚啤酒，龙虾，小烧烤
        //周四 ： 主动帮新来到女程序员解决bug
        //周五 ： 今晚吃鸡
        //周六 ： 与王婆介绍的小芳相亲
        //周日 ： 郁郁寡欢，准备上班
        Scanner sc = new Scanner(System.in) ;
        System.out.println("请输入周几：");
        System.out.println("周一请输入1 周二请输入2 以此类推");
        int  weekday = sc.nextInt();
        switch (weekday){
            case 1:
                System.out.println("埋头苦干，解决bug");
                break;
            case 2 :
                System.out.println("请大牛程序猿帮忙");
                break;
            case 3 :
                System.out.println("今晚啤酒，龙虾，小烧烤");
                break;
            case 4 :
                System.out.println("主动帮新来到女程序员解决bug");
                break;
            case 5 :
                System.out.println("今晚吃鸡");
                break;
            case 6 :
                System.out.println("与王婆介绍的小芳相亲");
                break;
            case 7 :
                System.out.println("郁郁寡欢，准备上班");
                break;
            default:
                System.out.println("请输入周一到周日中的其中一个！");
        }
    }
}
