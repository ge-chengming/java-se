package com.itheima.randm;

import java.util.Random;

public class RandomDemo1 {
    public static void main(String[] args) {
        //学会使用java提高的随机数类Random
        //1.导包
        //2.创建一个随机数对象
        Random r = new Random() ;
        //3。调用 nextInt功能 （方法）,可以返回一个整形的随机数给你
        for (int i = 0 ;i < 20 ; i++) {
            int data = r.nextInt(10) ;
            System.out.println(data);
        }

        System.out.println("--------------------");
        //1——10 ==》减加法
        //1-10 ==》 -1 <==> (0 -9) +1

        for (int i = 0 ; i < 10 ; i++) {
            int data2 = r.nextInt(15) + 3 ;
            System.out.println(data2);
        }
    }
}
