package com.itheima.variable;

public class VariableDemo3 {
    public static void main(String[] args) {
        //目标：掌握使用基本数据类型定义不同的变量
        System.out.println("------------基本数据类型-------------");
        //1.整形
        //1.1 byte字节型  占一个字节  -128 ~ 127
        byte number = 98;
        System.out.println(number);
//        byte number2 = 128;//错误一：不在数据范围内

        //1.2 short短整形 占2个字节
        short money = 30000;

        //1.3 int 整型 占4个字节
        int it = 1000000000;

        //1.4 long 长整型 占8个字节
//        long lg = 1222222222222222222222222222;//错误二：原因如下
        //注意，随便写一个字面量默认值是int类型
        //注意，如果需要随便写一个整数字面量当成long类型，需要在其后加L/l
        long lg2 = 122222222222222l;

        //2.浮点型（小数）
        //2.1 float单精度，占4字节
//        float score = 98.5;//错误三：原因看下面
        //注意，随便写一个小数字面量默认是double类型
        //如果希望随便写一个小数字面量，就在其后加一个F/f
        float score = 98.5F;

        //2.2 double双精度 占8个字节
        double score2 = 9999.9;

        //3.字符类型 char
        char ch = 'a';
        char ch2 = '中';

        //4.布尔类型
        boolean rs = true;
        boolean rs2 = false;

        System.out.println("-------------引用数据类型----------------");
        String name = "西门吹雪";
        //String 字符串类型
        System.out.println(name);
    }
}
