package com.itheima.variable;

public class VariableDemo1 {
    public static void main(String[] args) {
        //目标：学会使用变量
        //1.声明语法 ：数据类型 变量名称 = 初始值；
        //小数（double） 钱包（money） = 6院（6.0）
        double money = 6.0;
        System.out.println(money);

        //收红包
        //收四块
        //2.变量值相加(从右边往左边看)
        money = money + 4.0;
        System.out.println(money);

        System.out.println("------------------------------");
        //打印分割线

        //整型（int）年龄（age）21岁（21）
        //3.变量值直接更改
        int age = 21;
        System.out.println(age);
        age = 25 ;
        System.out.println(age);

    }
}
