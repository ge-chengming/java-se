package com.itheima.variable;

public class VariableDemo2 {
    public static void main(String[] args) {
        //目标：需要同学们理解变量使用的注意事项

        //1。变量先声明再使用
        int a = 23 ;
        System.out.println(a);

        //2.变量声明后，不能使用其他类型的数据
//        a = 1.5 ;//错误一：类型必须要是int

        /*
        3.变量的有效范围是从定义开始到“}”截止，
        且在同一个范围内部不能定义2个同名的变量
         */
        {
            int b = 25 ;
            System.out.println(b);
//            int b = 100;//错误二：b变量已经被定义了
        }
//        System.out.println(b);//错误三：b这个变量只在{}代码块内有效

        //4.定义代码的时候可以没有初始值，但是使用的时候必须要有初始值
        int c ;//无初始值直接定义
//        System.out.println(c);//错误三：使用前没有初始化
    }
}
