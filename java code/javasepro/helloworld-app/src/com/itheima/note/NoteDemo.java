package com.itheima.note;

/**
       目标：学会使用注释
       这个程序就是解释注释的
 */
public class NoteDemo {
    public static void main(String[] args) {
        //以上是一个打印语句
        /*
        窗前明月光
        疑是地上霜
        低头望明月
         */
        /*System.out.println("我开始学习Java程序，好嗨皮~~");
        System.out.println("我开始学习Java程序，好嗨皮~~");
        System.out.println("我开始学习Java程序，好嗨皮~~");
        System.out.println("我开始学习Java程序，好嗨皮~~");*/
        System.out.println("我开始学习Java程序，好嗨皮~~");
    }
}
