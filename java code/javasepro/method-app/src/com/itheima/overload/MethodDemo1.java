package com.itheima.overload;

public class MethodDemo1 {
    public static void main(String[] args) {
    //目标 ：识别方法重载的形式并且理解其调用流程
    //最后需要知道使用方法重载的好处
        //方法重载的识别技巧
        //1.只要是同一个类中，方法名称相同，参数列表不同，那就是重载的方法
        //2.形参列表不同指的是：形参的个数，类型，顺序不同，名称无所谓
        fire();
        fire("岛国");
        fire("岛国",100000);
    }

    public static void fire(){
//        System.out.println("默认发射一枚武器给米国~~~");
        fire("米国");
    }

    public static void fire(String location){
//        System.out.println("默认发射一枚武器给"+ location +"~~~");
        fire("岛国");
    }

    public static void fire(String location,int number){
        System.out.println("默认发射" + number + "枚武器给"+ location +"~~~");
    }
}


