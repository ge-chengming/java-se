package com.itheima.overload;

public class MethodDemo2 {
    public static void open(){}
    //新方法
    public static void open(int a){}
    //重载方法
    public static void open(int a ,int b){}
    //重载方法
    public static void open(double a ,int b){}
    //重载方法
    public static void open(int a ,double b){}
    //重载方法
//    public static void open(int i ,double d){}
    //不是重载，都是2个参数，都是先int后double
    public static void OPEN(){}
    //不是重载，是个新方法
}
