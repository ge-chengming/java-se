package com.itheima.demo;

public class Test3 {
    public static void main(String[] args) {
        //需求 ：使用方法，支持找出任意整形数组的最大值返回
        int[] ages = {23,19,25,78,34};
        int max = getArrayMaxData(ages);
        System.out.println("最大值数据是：" + max);
    }

    public static int getArrayMaxData (int[] arr){
        //找出数组的最大值进行返回

        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max){
                max = arr[i];
            }
        }
        return max ;
    }

}
