package com.itheima.creat;

public class MethodDemo3 {
    public static void main(String[] args) {
        //目标： 学会方法定义的其他形式，并理解其流程
        print();
        //无参数无返回值的方法的调用
    }

    /**
     无参数，无返回值的方法的定义
     */

    public static void print(){
        for (int i = 0; i < 3; i++) {
            System.out.println("helloworld");
        }
    }

    //注意事项
    //如果方法不需要返回结果，返回值类型必须声明成void（无返回值），此时方法内部不可以使用return返回数据
}









