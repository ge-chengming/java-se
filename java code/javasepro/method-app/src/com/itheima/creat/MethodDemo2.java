package com.itheima.creat;

public class MethodDemo2 {
    public static void main(String[] args) {
        //目标 ：学习方法的完整定义格式，并理解其调用和执行流程
        int rs = add(100,200);
        //1.方法的调用
        //数据类型 变量名 = 方法名（参数）【把return值给一个变量
        System.out.println("和是：" + rs);
    }

    public static int add(int a ,int b){
        int c = a + b ;
        return c ;
        //2.方法的完整定义格式
        //方法修饰符号（public static） 返回值类型 方法名称 （形参列表）{
        //             方法的执行代码
        //             return 返回值 ；
        // }
    }
}
