package com.itheima.creat;

public class MethodAttentionDemo4 {


    public static void main(String[] args) {
        /**
         1.方法编写顺序无所谓
         2.方法与方法之间是平级关系，不能嵌套定义
           即方法不能放在方法里面
         3.方法的返回值类型为void（无返回值），方法内就不能使用return放回数据
           芳法的返回值类型如果写了具体的类型，方法内部就必须要用return返回数据
         4.return语句下面，不能编写代码，因为永远执行不到，属于无效代码
         5.方法不调用就不会执行，调用时必须严格匹配方法的参数情况
         5.有返回值的方法调用时可以选择定义变量接受数据，或者直接输出调用，甚至直接调用
           五返回值的方法的调用只能直接调用
         */

        System.out.println(sum(10, 20));//直接输出返回值

        int rs = sum(100,900);//定义变量接收返回值
        System.out.println(rs);

        sum(1000,2000);//直接调用，方法还是在调用,不显示罢了

        print();//无返回值的方法直接调用

    }

    public static int sum (int a ,int b){
        return a + b ;
    }


    public static void print(){
        for (int i = 0; i < 3; i++) {
            System.out.println("helloword");
        }
    }



}
