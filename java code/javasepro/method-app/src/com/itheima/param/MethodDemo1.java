package com.itheima.param;

public class MethodDemo1 {
    public static void main(String[] args) {
        //目标：理解Java的基本类型的参数传递：值传递
        int a = 10 ;
        change(a);
        System.out.println(a);
    }

    public static void change(int a){
        System.out.println(a);
        a = 20 ;
        System.out.println(a);
    }
}

/**
 Java基本类型的参数传递是值传递
 只是把实参的值传递给了形参
 实参自身没有改变
 */
