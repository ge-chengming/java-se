package com.itheima;


import java.util.Random;
import java.util.Scanner;

/**
 需求：限制用户输入的红球号码不在（1-33），蓝球号码不在（1-16）内.并且不能重复！
 这也是个不小的需求。可以考虑使用方法来封装，毕竟逻辑是独立的。
 */


public class Test7BetterEnhanceByMyself {
    public static void main(String[] args) {
        //需求：模拟双色球【扩展案例】
        //业务分析，随机生成一组中奖号码
        //用户输入一组双色球号码
        //判断中奖情况

        // 1.随机6个红色球号码（1-33，不能重复），随机生成一个蓝色球号码（1-16），可以采用数组封装起来作为中奖代码
        int[] LuckNumbers =  creatLuckNumber();
//        printArray(LuckNumbers);
        //2.调用一个方法让用户输入7个号码，作为用户选号
        int[] userNumbers = userInputNumber();
//        printArray(userNumbers);
        if (userNumbers == null)
            return;
        //3.判断中奖情况
        judge(LuckNumbers,userNumbers);
    }

    public static int[] creatLuckNumber(){
        //本方法：随机6个红色球号码（1-33，不能重复），随机生成一个蓝色球号码（1-16），可以采用数组封装起来作为中奖代码
        //a. 定义一个动态初始化的数组 存储7个数字
        int[] numbers = new int[7];      //[0 0 0 0 0 0 0  | 0// ]
        //                                          i
        Random r = new Random();
        //b. 遍历数组，为每个位置生成对应的号码
        // ps : 遍历前6个位置，生成6个不重复的红球号码，范围时1-33

        for (int i = 0; i < numbers.length - 1; i++) {

            //c. ps 必须判断当前随机的号码之前是否出现过，如果出现过就要重新随机一个直到不重复

            // 为当前项目找出一个不重复的1-33之间的数字

            while (true){
                int data = r.nextInt(33) + 1 ;

                //定义一个flag变量，默认data是没有重复的

                boolean flag = true ;

                for (int j = 0; j < i ; j++) {
                    if (numbers[j] == data){
                        //说明 data这个数据 之前就出现过了,不能用
                        flag = false;
                        break;
                    }
                }

                if (flag){
                    //data 这个数据之前没有出现过，可以继续使用了
                    numbers[i] = data ;
                    break;
                }
            }
        }
        //d.为第七个位置生成一个1-16的号码作为蓝色号码
        numbers[numbers.length - 1] = r.nextInt(16) + 1 ;

        return numbers ;
    }

    public static int[] userInputNumber(){
        //本方法让用户输入7个号码，作为用户选号
        // a。定义一个数组存储7个号码
        int[] numbers = new int[7];
        //b.让用户录入6个红球号码
        Scanner sc = new Scanner(System.in);

//  再用户输入后加入拦截程序
        //如果用户输入的数字重复或者不在（1-33）便输出一个空数组，在main方法中去再次识别，结束方法
        for (int i = 0; i < numbers.length - 1 ; i++) {
            System.out.println("请您输入第" + (i + 1) + "个红球号码[(1-33),不重复】：");
            int data = sc.nextInt();
            //c.把当前录入的数据存入到数组中去

            int[] judge = judge(data);
            if (judge ==  null){
                return null;
            }
            //拦截用户输入的错误范围的号码

            numbers[i] = data;

            //-------------------------------------------------------
            for (int j = 0; j < i ; j++) {
                if (numbers[i] == numbers[j]){
                    System.out.println("error2");
                    return null;
                }
            }

            //拦截用户输入重复的数字
            //--------------------------------------------------------
        }


        System.out.println("请您输入一个篮球号码（1-16）");
        numbers[6] = sc.nextInt();
        //-------------------------------------------------------
        if (numbers[6] > 33 || numbers[6] < 1){
            System.out.println("error3");
            return null;
            //return null 去报错
        }
        //拦截用户输入不正确的数字
        //-------------------------------------------------------
        return numbers;
    }

    public static void judge(int[] LuckNumbers ,int[] userNumbers ){
        //本方法判断是否中奖
        // next is example
        //LuckNumber = 12 23  8  16 15 32   |  9
        //userNumber = 23 13 18  6  8  33   |  10
        //a。定义2个变量分别存储红球命中的个数，以及篮球命中的个数
        int redHitNumbers = 0 ;
        int blueHitNumbers = 0 ;

        //b.判断红球命中了几个，开始统计
        for (int i = 0; i < userNumbers.length - 1; i++) {
            for (int i1 = 0; i1 < LuckNumbers.length - 1; i1++) {

                //每次找到了相等，意味着当前号码命中了

                if (userNumbers[i] == LuckNumbers[i1]){
                    redHitNumbers++;
                    break;
                }
            }
        }

        //c.判断篮球号码是否命中
        blueHitNumbers = LuckNumbers[6] == userNumbers[6] ? 1 : 0 ;


        // 输出用户的投注情况和中奖号码的情况
        System.out.println("中奖号码是：" );
        printArray(LuckNumbers);
        System.out.println("您投注的号码是 ：");
        printArray(userNumbers);
        System.out.println("您命中的红球个数：" + redHitNumbers);
        System.out.println("您是否命中蓝球" + (blueHitNumbers == 1 ? "命中" : "没命中"));

        //真正开始判断中奖情况了
        if (blueHitNumbers == 1 && redHitNumbers < 3){
            System.out.println("恭喜你，中了5元小奖!");
        }else if (blueHitNumbers == 1 && redHitNumbers == 3 || blueHitNumbers == 0 && redHitNumbers ==4){
            System.out.println("恭喜你，中了10元小奖!");
        }else if (blueHitNumbers == 1 && redHitNumbers == 4 || blueHitNumbers == 0 && redHitNumbers ==5) {
            System.out.println("恭喜你，中了200元!");
        }else if (blueHitNumbers == 1 && redHitNumbers == 5) {
            System.out.println("恭喜你，中了3000元大奖!");
        }else if (blueHitNumbers == 0 && redHitNumbers == 6) {
            System.out.println("恭喜你，中了500万超级大奖!");
        }else if (blueHitNumbers == 1 && redHitNumbers == 6) {
            System.out.println("恭喜你，中了1000万巨奖!，可以开始享受人生，诗和远方~");
        }else{
            System.out.println("谢谢你为福利事业做出的突出贡献！！！");
        }
    }



    public static void printArray(int[] arr){
        //本方法用来打印一个数组的所有数值
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "\t");
        }
        System.out.println();//换行
    }

    public static int[] judge(int data){
        //-------------------------------------------------------
        if (data > 33 || data < 1){
            System.out.println("error1");
            return null;
            //return null 去报错
        }
        //拦截用户输入不正确的数字
        //-------------------------------------------------------
        int[] niceeee ={0};
        return niceeee;
    }


}
