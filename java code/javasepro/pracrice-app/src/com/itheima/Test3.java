package com.itheima;

import java.util.Random;

/**
 需求： 定义一个方法实现随机产生与一个5为验证码，每位数字可以是数字，大写字母，小写字母
 */
public class Test3 {
    public static void main(String[] args) {
        //4.调用获取验证码的方法得到一个验证码
        String code = creatcode(5);
        System.out.println(code);
    }

    //1.定义一个方法返回一个随机验证码，
    //返回值类型 String  形参 int n

    public static String creatcode (int n){
        String code = "";
        //2.定义一个for循环，循环n次，依此生成随机字母
        Random r = new Random();
        for (int i = 0; i < n; i++) {
            //3.生成一个随机字符
            //3.1.先随机一个 0，1，2
            //如果是0 就去随机大写字母 ；如果是1 就去随机小写字母 ； 如果是2 就去随机数字
            int type = r.nextInt(3);
            switch (type){
                case 0 :
                    // 大写字母 (A 65 Z 65 + 25)
                    char ch = (char)(r.nextInt(26) + 65);
                    //int类型强制转为char 可以编码ascll码，把数字变为字符
                    //格式 char 变量名 = （char）（int 变量名）
                    code += ch;
                    break;
                case 1 :
                    // 小写字母(a 97 z 97 + 25)
                    char ch1 = (char)(r.nextInt(26) + 97);
                    //int类型强制转为char 可以编码ascll码，把数字变为字符
                    //格式 char 变量名 = （char）（int 变量名）
                    code += ch1;
                    break;
                case 2 :
                    // 数字
                    code += r.nextInt(10);
                    break;
            }
        }
        return code;
    }
}



