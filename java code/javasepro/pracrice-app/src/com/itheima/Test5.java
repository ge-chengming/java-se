package com.itheima;

import java.util.Scanner;

/**
 需求： 在唱歌比赛最中；有6名评委给选手打分，分数范围是【0-100】之间的整数。
 选手最后的得分为;去掉最低分和去掉最高分后的平均分
 完成上述过程并且计算出选手的得分
 */
public class Test5 {
    public static void main(String[] args) {
        //1.定义一个动态初始化的数组，用于后期录入6个评委的分数
        Scanner sc = new Scanner(System.in);
        System.out.println("please input the number of judger");
        int number = sc.nextInt();
        int[] scores = new int[number];
        //2.录入n个评委的分数
        for (int i = 0; i < scores.length ; i++) {
            System.out.println("请您输入第" + (i + 1) + "个评委的打分；");
            int score = sc.nextInt();
            if(score > 100 || score < 0){
                System.out.println("error");
                break;
            }
            //3.把这个分数存入到对应的位置处
            scores[i] = score ;
        }
        //3,遍历数组的每个数据，找出最高分和最低分和总分
        int max = scores[0];
        int min = scores[0];
        int sum = 0;
        for (int i = 0; i < scores.length; i++) {
            if(scores[i] > max){
                max = scores[i];
            }
            if(scores[i] < min){
                min = scores[i];
            }
            sum += scores[i];
        }
        System.out.println("max score is :" + max);
        System.out.println("min score is :" + min);
        //4.统计平均分
        double result = (sum - max -min) * 1.0 / (scores.length - 2);
        System.out.println("the result is :" +  result);
    }
}


