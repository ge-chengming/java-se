package com.itheima;

import java.util.Scanner;

public class Test6 {
    public static void main(String[] args) {
        //需求；数据加密
        //1.定义一个数组存入需要加密的数据
        System.out.println("请您输入需要加密的数字的个数：");
        Scanner sc = new Scanner(System.in);
        int length = sc.nextInt();
        int[] arr = new int[length];

        // 2.录入需要加密的数字
        for (int i = 0; i < arr.length; i++) {
            System.out.println("请你输入加密的第" + (i + 1) + "个数字");
            int number = sc.nextInt();
            arr[i] = number;
        }

        //3.打印数组内容
        printArray(arr);

        //4.核心逻辑，对数组中的数据加密
        //每各数字加5 再除十取余数 最后把所得到的数字倒装
        //4,1 每位数字加五再除以10取余数
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (arr[i] + 5) % 10 ;
        }
        //4.2 倒装
        for (int i = 0 ,  j = arr.length - 1 ; i < j; i++ ,j --) {
            // 直接交换两者位置的值
            int tmep = arr[j];
            arr[j] = arr[i];
            arr[i] = tmep;
        }
        printArray(arr);
    }

    public static void printArray (int[] arr){
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(i == (arr.length - 1) ? arr[i] : arr[i] + ",");
        }
        System.out.println("]");
    }
}


