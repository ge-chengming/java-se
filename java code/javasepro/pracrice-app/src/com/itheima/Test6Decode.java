package com.itheima;

import java.util.Scanner;

public class Test6Decode {
    public static void main(String[] args) {
        //录入用户想要解密的数字位数
        System.out.println("Enter the number of numbers you want to encrypt ");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        //录入用户想要解密的数字
        int[] arr = new int[number];
        for (int i = 0; i < number; i++) {
            System.out.println("please input the" + (i + 1) + "number" );
            arr[i] = sc.nextInt();
        }
        //打出用户输入的数据
        printArray(arr);
        //核心解密部分
        //反转数字

        for (int i = 0 ; i < arr.length - 1 - i; i++ ) {
            // 直接交换两者位置的值
            int tmep = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = arr[i];
            arr[i] = tmep;
        }

        //打出被反转的数据
        printArray(arr);

        //逆转思维破解
        //逆转思维破解
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] >= 0 && arr[i] <= 4){
                arr[i] = arr[i] + 10 ;
            }else if (arr[i] > 9 && arr[i] < 0){
                System.out.println("error!!!");
                return;
            }
            arr[i] -= 5;
        }
        //打出最终结果
        printArray(arr);
    }

    public static void printArray (int[] arr){
        System.out.print("[");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(i == arr.length - 1 ? arr[i] : arr[i] + ",");
        }
        System.out.println("]");
    }
}
