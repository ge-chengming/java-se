package com.itheima.type;

public class TypeDemo2 {
    public static void main(String[] args) {
        //目标：表达式的自动类型转换的规则
        //小范围类型的变量会自动转换成较大范围的类型再运算
        // byte,short,char -> int -> long -> float -> double
        //注意事项
        //1.表达式的最终结果类型是由表达式的最高型决定的
        //2.在表达式中，byte，short，char 是直接转换成int类型参与运算
        byte a = 10 ;
        int b = 20 ;
        double c = 1.0;
        double rs = a + b + c;
//        float rs2 = a + b + c;//错误一：要用最高型
        System.out.println(rs);

        double rs2 = a + b -2.3 ;
        System.out.println(rs2);

        //面试题
        byte i = 10 ;
        byte j = 20 ;
        // i + j 用什么数据去接
//        byte k = i + j ;
        //错误答案：运算时byte自动转换为int导致最终结果为int
        int k  = i + j ;
        System.out.println(k);
        //正确答案
    }

}
