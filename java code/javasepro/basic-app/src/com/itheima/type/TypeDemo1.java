package com.itheima.type;

public class TypeDemo1 {
    public static void main(String[] args) {
        //目标：理解自动类型的转换
        /*
        byte -> short -> int -> long -> float -> double
                    char ->int
         */

        byte a = 20 ;
        int b = a ;
        //发生了自动类型转换
        //小的数据类型自动赋值给大的数据类型
        System.out.println(a);
        System.out.println(b);

        int age = 23 ;
        double db = age ;
        // 自动类型转换
        System.out.println(db);

        char ch = 'a';
        int code = ch ;
        //自动类型转换
        System.out.println(code);
        //打印值为97.即char 'a' 的ASCII码

    }
}
