package com.itheima.type;

public class TypeDemo3 {
    public static void main(String[] args) {
        //目标：理解强制类型转换，并使用
        //注意事项
        //1.强制类型转换可能会导致数据（丢失）溢出
        //2.浮点型强制类型转换为整形，直接丢掉小数部分，保留整数部分返回
        int a = 20 ;
//        byte b = a ;//错误一：大范围变量不能给小范围变量

        //强制类型转换
        //数据类型 变量2 = （数据类型）变量1、数据
        byte b = (byte)a ;
        System.out.println(b);
        System.out.println(a);

        System.out.println("---------注意事项1---------");

        int i = 1500;
        byte j = (byte) i;
        //快捷将 Alt + Enter 万能提示键
        System.out.println(j);
        //数据丢失
        // i = 00000000 00000000 00000101 11011100
        // i 被强行转换为byte时 截取后8位
        // i = 11011100
        // 第一位 1 代表 负数

        System.out.println("--------注意事项2-----------");

        double score = 99.5 ;
        int it = (int) score;
        System.out.println(it);
        //保留了整数部分，小数部分被丢掉了

    }
}
