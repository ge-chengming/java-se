package com.itheima.scanner;
// 1.导包操作
import java.util.Scanner;

public class ScannerDemo {
    public static void main(String[] args) {
        //目标 ： 学会使用键盘录入技术
        //2.得到一个键盘扫描器对象
        Scanner sc = new Scanner(System.in);

        //3.调用sc对象的功能等待接受用户的输入的数据
        System.out.println("请您输入你的年龄：");
        int age = sc.nextInt() ;
        System.out.println("您的年龄是：" + age );

        System.out.println("请您输入你的名字：");
        String name = sc.next() ;
        System.out.println("欢迎" + name );
        //这个代码会等待用户输入数值，直到用户输入数值并且按下回车就会把数据拿到
    }
}
