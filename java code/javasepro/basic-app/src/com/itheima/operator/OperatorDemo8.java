package com.itheima.operator;

public class OperatorDemo8 {
    public static void main(String[] args) {
        //目标：学会使用三元运算符，理解其流程
        // 表达式 ？ 值1 ： 值2 ；（true回值1）
        double score = 98 ;
        String rs = score >= 60 ? "考试通过" : "考试不通过" ;
        System.out.println(rs);

        //需求，从两个整数中找出较大值
        int a = 10 ;
        int b = 2000 ;
        int max = a >= b ? a : b ;
        System.out.println(max);

        System.out.println("-----------------");
        int i = 10 ;
        int j = 30 ;
        int k = 50 ;
        //first 找出两个整数的较大值
        int temp = i > j ? i : j ;
        // second 临时变量temp的值拿去和k进行比较
        int rsMax = temp >= k ? temp : k ;
        System.out.println(rsMax);

        System.out.println("---------------------------");
        int rsMax1 = i > j ? (i > k ? i : k) : (j > k ? j : k) ;
        System.out.println(rsMax1);
        //三元运算符的嵌套

    }
}
