package com.itheima.operator;

public class OperatorDemo5 {
    public static void main(String[] args) {
        //目标：学会赋值运算符
        // += 加后赋值  a += b <===> a = (a的数据类型)（a + b）
        // -= 减后赋值  a -= b <===> a = (a的数据类型)（a - b）
        // *= 乘后赋值  a *= b <===> a = (a的数据类型)（a * b）
        // /= 除后赋值  a /= b <===> a = (a的数据类型)（a / b）
        // %= 余后赋值  a %= b <===> a = (a的数据类型)（a % b）
        //注意：赋值运算符自带强制类型转换

        int a = 10 ;
        int b = 200 ;
        System.out.println(a += b);

        byte i = 10 ;
        byte j = 20 ;
        System.out.println(i += j);
        // i += j  <==> i = (type)(i + j)

        int m = 10 ;
        int n = 5 ;
        System.out.println(m /= n);
        //m = 2
        System.out.println(m *= n);
        //m = 10
        System.out.println(m += n);
        //m = 15
        System.out.println(m -= n);
        //m = 10
    }
}
