package com.itheima.operator;

public class OperatorDemo1 {
    public static void main(String[] args) {
        //目标：掌握基本的算数运算符的使用
        // + - * / %
        //注意事项
        //1.除法时，整数除以整数结果还是整数

        int a = 10 ;
        int b = 3 ;

        System.out.println("a = 10");
        System.out.println("b = 3");

        System.out.println("---------加法--------");
        System.out.println(a + b);
        System.out.println("---------减法--------");
        System.out.println(a - b);
        System.out.println("---------乘法--------");
        System.out.println(a * b);
        System.out.println("---------除法--------");
        System.out.println(a / b);//3.3333 ==> 3
        System.out.println("---------取余--------");
        System.out.println(a % b);
        System.out.println("在int 类型的 a 前面乘以 1.0 \n由于表达式自动转换的规则\n变成了 double 类型 / int 类型");
        System.out.println(((a * 1.0) / (b)));

    }
}
