package com.itheima.operator;

public class OperatorDemo7 {
    public static void main(String[] args) {
        //目标：学会使用逻辑运算符，并能够选择合适的逻辑运算符解决需求
        // 逻辑与 & 左右都为true时为true
        // 逻辑或  | 左右一个为 true 时为true
        // 逻辑非 ！ ！true = false ； ！false = true
        // 逻辑异或 ^   两个不同为true
        // 短路与 && 判断结果与&一样，但是左边为false就不执行右边
        //短路或 ||  判断结果与|一样，但是左边为true就不止行右边
        double size = 9.8 ;
        double storage = 16 ;
        //需求：尺寸大于等于6.95，内存要大于等于8gb
        System.out.println(size >= 6.95 & storage >= 8);
        System.out.println(size >= 6.95 | storage >= 8);
        System.out.println(!(size >= 6.95 & storage >= 8));
        System.out.println(false ^ true);

        System.out.println("----------&& and ||-------------");
        int a = 10 ;
        int b = 20 ;
        System.out.println(a > 100 && ++b > 10);
        System.out.println(b);
        System.out.println(a > 100 & ++b > 10);
        System.out.println(b);
        //&& ++b 不执行
        //& ++b 继续执行

        int i = 10 ;
        int j = 20 ;
        System.out.println(i > 2 || ++j >10);
        System.out.println(j);
        System.out.println(i > 2 | ++j >10);
        System.out.println(j);
    }
}
