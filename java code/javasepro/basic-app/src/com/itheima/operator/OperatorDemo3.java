package com.itheima.operator;

public class OperatorDemo3 {
    public static void main(String[] args) {
        //目标：加符号做连接符的识别
        //注意：能算就算，不能就在一起
        int a = 5 ;
        System.out.println("abc" + "a");
        System.out.println("abc" + a);
        System.out.println(5 + a);
        System.out.println("abc" + 5 + 'a');
        System.out.println(15 + "abc" +15);
        System.out.println(a + 'a');
        //’a‘代表了97，所以可以和int a 进行计算
        System.out.println(a + "" + 'a');
        System.out.println(a + 'a' + "itheima");
        System.out.println("itheima" + a + 'a');
        System.out.println("itheima" + (a + 'a'));
    }
}
