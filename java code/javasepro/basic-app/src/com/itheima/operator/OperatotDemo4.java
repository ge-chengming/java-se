package com.itheima.operator;

public class OperatotDemo4 {
    public static void main(String[] args) {
        //目标：学会使用自增自减运算符
        // ++ --
        //注意事项
        /* ++ a 和 a ++ 在单独使用时一样的
         但是在表达式中，或者由其他操作时 会有明显的区别
         ++ a 是先对变量 +1 再拿变量的值进行运算
         a ++ 是先拿变量值进行计算，再对变量值+1
         */
        int a = 10 ;
        a ++ ;
        // a = a + 1
        System.out.println(a);
        ++ a ;
        System.out.println(a);

        int b = 10 ;
        b -- ;
        System.out.println(b);
        -- b ;
        System.out.println(b);

        System.out.println("--------------------------");
        //讨论 ++a 和 a ++ 的区别
        //具体内容键注意事项
        int i = 10 ;
        int j = ++i ;
        System.out.println(i);
        System.out.println(j);
        //++ a 是先对变量 +1 再拿变量的值进行运算

        int m = 10 ;
        int n = m ++ ;
        System.out.println(m);
        System.out.println(n);
        //a ++ 是先拿变量值进行计算，再对变量值+1

        System.out.println("---------扩展案例---------");
        int k = 3 ;
        int p = 5 ;
        //k 3 4 5 4
        //p 5 4 3 4
        //rs 3 + 5 - 4 + 4 - 5 + 4 + 2 = 9
        int s = k++ + ++k - --p + p-- - k-- + ++p + 2 ;
        System.out.println(k);
        System.out.println(p);
        System.out.println(s);

    }
}
